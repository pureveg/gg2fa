# Google 2 factor authenticator

## Get it
```go get -u code.google.com/p/rsc/qr```

```go get -u bitbucket.org/pureveg/gg2fa```

## Test it
cd bitbucket.org/pureveg/gg2fa && go test -v

## License
BSD License
