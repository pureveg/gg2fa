package gg2fa

import (
	"os"
	"testing"
)

func Test_Image(t *testing.T) {
	// generate image
	imgByte, err := BarcodeImage("user@site", []byte("randomString"), nil)
	if err != nil {
		t.Errorf("Got some error: %q", err)
	}

	// create image file
	f, err := os.Create("gg2fa.png")
	if err != nil {
		t.Errorf("Can not create image: %q", err)
	}

	// write byte to image
	_, err = f.Write(imgByte)
	if err != nil {
		t.Errorf("Can not write byte into image: %q", err)
	}
}

func Test_Authentication(t *testing.T) {
	authen := Authenticate([]byte("randomString"), "123456", nil)
	if authen {
		t.Errorf("Should not be authenticated")
	}
}
